# Latest rust alpine image
FROM --platform=$BUILDPLATFORM docker.io/messense/rust-musl-cross:x86_64-musl as builder-amd64
FROM --platform=$BUILDPLATFORM docker.io/messense/rust-musl-cross:aarch64-musl as builder-arm64

FROM --platform=$BUILDPLATFORM builder-${TARGETARCH} as builder
# Using musl instead of libc allows static linking.
# I.e. we can copy the binary to a scratch image.

WORKDIR /usr/src/algotrading
COPY . .

ARG TARGETARCH

RUN export ARCHNAME="x86_64" && \
    if [ "$TARGETARCH" = "arm64" ]; \
    then export ARCHNAME="aarch64"; \
    fi; \
    cargo install --target "${ARCHNAME}-unknown-linux-musl" --path ./experiment

FROM scratch
COPY --from=builder /root/.cargo/bin/experiment /experiment
CMD ["/experiment"]
EXPOSE 7878
