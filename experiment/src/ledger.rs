use async_trait::async_trait;
use cqrs_es::{Aggregate, DomainEvent};
use serde::{Deserialize, Serialize};
use std::fmt::{Debug, Display, Formatter};

/// The possible actions to perform on the ledger
#[derive(Debug, Deserialize)]
pub enum LedgerCommand {
    /// Open account
    OpenAccount {
        /// ID of the new account
        account_id: String,
    },
    /// Deposit money
    DepositMoney {
        /// Amount to deposit
        amount: f64,
    },
    /// Withdraw money, if possible
    WithdrawMoney {
        /// Amount to withdraw
        amount: f64,
    },
}

/// The possible ledger events
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub enum LedgerEvent {
    /// An account was opened
    AccountOpened {
        /// ID of account that was opened
        account_id: String,
    },
    /// Money was deposited
    CustomerDepositedMoney {
        /// Amount that was deposited
        amount: f64,
        /// Balance after deposit
        balance: f64,
    },
    /// Money was withdrawn from account
    CustomerWithdrewMoney {
        /// Amount that was withdrawn
        amount: f64,
        /// Balance after withdrawal
        balance: f64,
    },
}

impl DomainEvent for LedgerEvent {
    fn event_type(&self) -> String {
        let event_type: &str = match self {
            LedgerEvent::AccountOpened { .. } => "AccountOpened",
            LedgerEvent::CustomerDepositedMoney { .. } => "CustomerDepositedMoney",
            LedgerEvent::CustomerWithdrewMoney { .. } => "CustomerWithdrewMoney",
        };
        event_type.to_string()
    }

    fn event_version(&self) -> String {
        "1.0".to_string()
    }
}

/// Error type, e.g. to tell the caller not enough funds available
#[derive(Debug, PartialEq)]
pub struct LedgerError(String);

impl Display for LedgerError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::error::Error for LedgerError {}

impl From<&str> for LedgerError {
    fn from(message: &str) -> Self {
        LedgerError(message.to_string())
    }
}

/// External services must be called during the processing of the command.
#[async_trait]
pub trait LedgerApi: Sync + Send {
    /// API entry for withdrawal
    async fn make_withdrawal(&self, _amount: f64) -> Result<(), WithdrawError>;
    /// API entry for deposit
    async fn make_deposit(&self, _amount: f64) -> Result<(), DepositError>;
    /// API entry for checking ledger balance
    async fn check_balance(&self, _account: &str) -> Result<(), BalanceError>;

    /// Get Type name
    fn type_name(&self) -> String;
}

/// Examples of ledger functions
pub struct LedgerServices {
    /// Services provided by Ledger
    pub services: Box<dyn LedgerApi>,
}

impl Display for LedgerServices {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.services.type_name())
    }
}

impl Debug for LedgerServices {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.services.type_name())
    }
}

impl LedgerServices {
    /// Create new LedgerServices from API descriptor
    pub fn new(services: Box<dyn LedgerApi>) -> Self {
        Self { services }
    }
}

/// Placeholder for implementation of LedgerApi
#[derive(Debug, Clone, Copy)]
pub struct HappyPathLedgerServices;

#[async_trait]
impl LedgerApi for HappyPathLedgerServices {
    async fn make_withdrawal(&self, _amount: f64) -> Result<(), WithdrawError> {
        Ok(())
    }

    async fn make_deposit(&self, _amount: f64) -> Result<(), DepositError> {
        Ok(())
    }

    async fn check_balance(&self, _account: &str) -> Result<(), BalanceError> {
        Ok(())
    }

    fn type_name(&self) -> String {
        "HappyPath".to_string()
    }
}
/// Deposit ledger function error
#[derive(Debug, Clone, Copy)]
pub struct DepositError;
/// Withdrawal ledger function error
#[derive(Debug, Clone, Copy)]
pub struct WithdrawError;
/// Check balance ledger function error
#[derive(Debug, Clone, Copy)]
pub struct BalanceError;

/// Ledger object
#[derive(Debug, Clone, Copy, Serialize, Default, Deserialize)]
pub struct Ledger {
    opened: bool,
    // this is a floating point for our example, don't do this IRL
    balance: f64,
}

fn deposit_money(ledger: &Ledger, amount: f64) -> Vec<LedgerEvent> {
    let balance = ledger.balance + amount;
    vec![LedgerEvent::CustomerDepositedMoney { amount, balance }]
}

fn withdraw_money(ledger: &Ledger, amount: f64) -> Result<Vec<LedgerEvent>, LedgerError> {
    if ledger.balance < amount {
        return Err(LedgerError("Funds not available!".to_string()));
    }
    let balance = ledger.balance - amount;
    Ok(vec![LedgerEvent::CustomerWithdrewMoney { amount, balance }])
}

#[async_trait]
impl Aggregate for Ledger {
    type Command = LedgerCommand;
    type Event = LedgerEvent;
    type Error = LedgerError;
    type Services = LedgerServices;

    // This identifier should be unique to the system.
    fn aggregate_type() -> String {
        "Account".to_string()
    }

    // The aggregate logic goes here. Note that this will be the _bulk_ of a CQRS system
    // so expect to use helper functions elsewhere to keep the code clean.
    async fn handle(
        &self,
        command: Self::Command,
        _services: &Self::Services,
    ) -> Result<Vec<Self::Event>, Self::Error> {
        match command {
            LedgerCommand::DepositMoney { amount } => Ok(deposit_money(self, amount)),
            LedgerCommand::WithdrawMoney { amount } => withdraw_money(self, amount),
            _ => Ok(vec![]),
        }
    }

    fn apply(&mut self, event: Self::Event) {
        match event {
            LedgerEvent::AccountOpened { .. } => self.opened = true,

            LedgerEvent::CustomerDepositedMoney { amount: _, balance } => {
                self.balance = balance;
            }

            LedgerEvent::CustomerWithdrewMoney { amount: _, balance } => {
                self.balance = balance;
            }
        }
    }
}

#[cfg(test)]
mod aggregate_tests {
    use super::*;
    use crate::eventstore::SimpleLoggingQuery;
    use cqrs_es::{test::TestFramework, CqrsFramework};

    /// In-memory event-store
    use cqrs_es::mem_store::MemStore;
    // let event_store = MemStore::<Ledger>::default();

    type AccountTestFramework = TestFramework<Ledger>;

    #[test]
    fn test_deposit_money() {
        let expected = LedgerEvent::CustomerDepositedMoney {
            amount: 200.0,
            balance: 200.0,
        };

        let services = LedgerServices::new(Box::new(HappyPathLedgerServices));
        AccountTestFramework::with(services)
            .given_no_previous_events()
            .when(LedgerCommand::DepositMoney { amount: 200.0 })
            .then_expect_events(vec![expected]);
    }

    #[test]
    fn test_deposit_money_with_balance() {
        let previous = LedgerEvent::CustomerDepositedMoney {
            amount: 200.0,
            balance: 200.0,
        };
        let expected = LedgerEvent::CustomerDepositedMoney {
            amount: 200.0,
            balance: 400.0,
        };

        let services = LedgerServices::new(Box::new(HappyPathLedgerServices));
        AccountTestFramework::with(services)
            .given(vec![previous])
            .when(LedgerCommand::DepositMoney { amount: 200.0 })
            .then_expect_events(vec![expected]);
    }

    #[test]
    fn test_withdraw_money() {
        let previous = LedgerEvent::CustomerDepositedMoney {
            amount: 200.0,
            balance: 200.0,
        };
        let expected = LedgerEvent::CustomerWithdrewMoney {
            amount: 100.0,
            balance: 100.0,
        };

        let services = LedgerServices::new(Box::new(HappyPathLedgerServices));
        AccountTestFramework::with(services)
            .given(vec![previous])
            .when(LedgerCommand::WithdrawMoney { amount: 100.0 })
            .then_expect_events(vec![expected]);
    }

    #[test]
    fn test_withdraw_money_funds_not_available() {
        let services = LedgerServices::new(Box::new(HappyPathLedgerServices));
        AccountTestFramework::with(services)
            .given_no_previous_events()
            .when(LedgerCommand::WithdrawMoney { amount: 200.0 })
            .then_expect_error(LedgerError("Funds not available!".to_string()));
    }

    #[tokio::test]
    async fn test_event_store() {
        let event_store = MemStore::<Ledger>::default();
        let query = SimpleLoggingQuery {};
        let service = LedgerServices::new(Box::new(HappyPathLedgerServices));
        let cqrs = CqrsFramework::new(event_store, vec![Box::new(query)], service);

        let aggregate_id = "aggregate-instance-A";

        // deposit $1000
        cqrs.execute(
            aggregate_id,
            LedgerCommand::DepositMoney { amount: 1000_f64 },
        )
        .await
        .unwrap();

        // withdraw $236.15
        cqrs.execute(
            aggregate_id,
            LedgerCommand::WithdrawMoney { amount: 236.15 },
        )
        .await
        .unwrap();
    }
}
