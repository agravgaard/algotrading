//! The main entrypoint for the app

#![doc(html_root_url = "https://gitlab.com/agravgaard/algotrading")]
#![forbid(unsafe_code)]
#![deny(
    absolute_paths_not_starting_with_crate,
    elided_lifetimes_in_paths,
    explicit_outlives_requirements,
    keyword_idents,
    macro_use_extern_crate,
    meta_variable_misuse,
    missing_abi,
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    non_ascii_idents,
    noop_method_call,
    pointer_structural_match,
    single_use_lifetimes,
    trivial_casts,
    trivial_numeric_casts,
    unreachable_pub,
    unsafe_op_in_unsafe_fn,
    unstable_features,
    unused_crate_dependencies,
    unused_extern_crates,
    unused_import_braces,
    unused_lifetimes,
    unused_qualifications,
    unused_results,
    variant_size_differences
)]
#![warn(
    clippy::panic,
    future_incompatible,
    nonstandard_style,
    rust_2018_idioms
)]

use axum::{
    http::StatusCode,
    response::IntoResponse,
    routing::{get, post},
    Json, Router,
};
use serde::{Deserialize, Serialize};
use std::net::SocketAddr;

use quant::*;

/// CQRS database interface, i.e. the eventstore
pub mod eventstore;
/// A CQRS & Event Sourcing based ledger to manage available funds
pub mod ledger;

#[tokio::main]
async fn main() {
    // initialize tracing
    tracing_subscriber::fmt::init();

    // build our application with a route
    let app = Router::new()
        // `GET /` goes to `root`
        .route("/", get(root))
        // `POST /dpremium` goes to `post_delta_premium`
        .route("/dpremium", post(post_delta_premium))
        .route("/bscall", post(post_greeks_call))
        .route("/bsput", post(post_greeks_put));

    // run our app with hyper
    // `axum::Server` is a re-export of `hyper::Server`
    let addr = SocketAddr::from(([127, 0, 0, 1], 7878));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

// basic handler that responds with a static string
async fn root() -> &'static str {
    "Hello, World!"
}

// placeholder for calling quant lib
async fn post_delta_premium(
    // this argument tells axum to parse the request body
    // as JSON into a `CreateUser` type
    Json(payload): Json<DeltaPremiumArgs>,
) -> impl IntoResponse {
    // insert your application logic here
    let result = DeltaPremiumResult {
        premium: delta_premium(payload.delta, payload.diff),
    };

    // this will be converted into a JSON response
    // with a status code of `200 OK`
    (StatusCode::OK, Json(result))
}

// the input to our `create_user` handler
#[derive(Deserialize)]
struct DeltaPremiumArgs {
    delta: f64,
    diff: f64,
}

// the output to our `create_user` handler
#[derive(Serialize)]
struct DeltaPremiumResult {
    premium: f64,
}

// get call premium and greeks from quant lib
async fn post_greeks_call(
    // this argument tells axum to parse the request body
    // as JSON into a `CreateUser` type
    Json(payload): Json<OptionArgs>,
) -> impl IntoResponse {
    let option = blackscholes::Option {
        x: payload.x,
        sigma: payload.sigma,
        r: payload.r,
        b: payload.b,
    };
    let t = payload.t;
    let s = payload.s;
    let premium = option.black_scholes_call().expect("epic fail");
    // insert your application logic here
    let result = GreeksResult {
        premium: premium(t, s),
        delta: option.delta_call(t, s).expect("Delta fail!"),
        gamma: option.gamma(t, s).expect("gamma fail!"),
        theta: option.theta_call(t, s).expect("theta fail!"),
        vega: option.vega(t, s).expect("vega fail!"),
        rho: option.rho_call(t, s, false).expect("rho fail!"),
        elasticity: option.elasticity_call(t, s).expect("elasticity fail!"),
        vanna: option.vanna(t, s).expect("vanna fail!"),
        charm: option.charm_call(t, s).expect("charm fail!"),
        speed: option.speed(t, s).expect("speed fail!"),
        color: option.color(t, s).expect("color fail!"),
        volga: option.volga(t, s).expect("volga fail!"),
        vega_decay: option.vega_decay(t, s).expect("vega_decay fail!"),
        zomma: option.zomma(t, s).expect("zomma fail!"),
    };

    // this will be converted into a JSON response
    // with a status code of `200 OK`
    (StatusCode::OK, Json(result))
}

// get put premium and greeks from quant lib
async fn post_greeks_put(
    // this argument tells axum to parse the request body
    // as JSON into a `CreateUser` type
    Json(payload): Json<OptionArgs>,
) -> impl IntoResponse {
    let option = blackscholes::Option {
        x: payload.x,
        sigma: payload.sigma,
        r: payload.r,
        b: payload.b,
    };
    let t = payload.t;
    let s = payload.s;
    let premium = option.black_scholes_put().expect("epic fail");
    // insert your application logic here
    let result = GreeksResult {
        premium: premium(t, s),
        delta: option.delta_put(t, s).expect("Delta fail!"),
        gamma: option.gamma(t, s).expect("gamma fail!"),
        theta: option.theta_put(t, s).expect("theta fail!"),
        vega: option.vega(t, s).expect("vega fail!"),
        rho: option.rho_put(t, s, false).expect("rho fail!"),
        elasticity: option.elasticity_put(t, s).expect("elasticity fail!"),
        vanna: option.vanna(t, s).expect("vanna fail!"),
        charm: option.charm_put(t, s).expect("charm fail!"),
        speed: option.speed(t, s).expect("speed fail!"),
        color: option.color(t, s).expect("color fail!"),
        volga: option.volga(t, s).expect("volga fail!"),
        vega_decay: option.vega_decay(t, s).expect("vega_decay fail!"),
        zomma: option.zomma(t, s).expect("zomma fail!"),
    };

    // this will be converted into a JSON response
    // with a status code of `200 OK`
    (StatusCode::OK, Json(result))
}

// the input to our `create_user` handler
#[derive(Deserialize)]
struct OptionArgs {
    /// Exercise / Strike price of the option
    x: f64,
    /// Risk-free interest rate
    r: f64,
    /// The volatility (SD) of the undelying [0, 1]
    sigma: f64,
    /// b=r for stocks,
    /// b=r=0 for futures with futures-type settlement,
    /// b=0 for futures with stock-type settlement,
    /// b=r-rf on foreign currencies
    b: f64,
    /// Price of the underlying
    s: f64,
    /// Time to expiration in years
    t: f64,
}

// the output to our `create_user` handler
#[derive(Serialize)]
struct GreeksResult {
    premium: f64,
    delta: f64,
    gamma: f64,
    theta: f64,
    vega: f64,
    rho: f64,
    elasticity: f64,
    vanna: f64,
    charm: f64,
    speed: f64,
    color: f64,
    volga: f64,
    vega_decay: f64,
    zomma: f64,
}
