use async_trait::async_trait;
use cqrs_es::{persist::GenericQuery, EventEnvelope, Query, View};
use postgres_es::{PostgresCqrs, PostgresViewRepository};
use serde::{Deserialize, Serialize};
use sqlx::{Pool, Postgres};
use std::fmt::Debug;
use std::sync::Arc;

use crate::ledger::{HappyPathLedgerServices, Ledger, LedgerEvent, LedgerServices};

/// Struct for querying and printing the event history
#[derive(Debug, Clone, Copy)]
pub struct SimpleLoggingQuery {}

#[async_trait]
impl Query<Ledger> for SimpleLoggingQuery {
    async fn dispatch(&self, aggregate_id: &str, events: &[EventEnvelope<Ledger>]) {
        for event in events {
            let payload =
                serde_json::to_string_pretty(&event.payload).expect("Could not stringify payload!");
            println!("{}-{}\n{:#?}", aggregate_id, event.sequence, payload);
        }
    }
}

/// This one will be handled with Postgres `GenericQuery`
/// which will serialize and persist our view after it is updated. It also
/// provides a `load` method to deserialize the view on request.
pub type AccountQuery =
    GenericQuery<PostgresViewRepository<LedgerView, Ledger>, LedgerView, Ledger>;

/// The view for a Ledger query, for a standard http application this should
/// be designed to reflect the response that will be returned to a user.
#[derive(Debug, Default, Serialize, Deserialize)]
pub struct LedgerView {
    account_id: Option<String>,
    balance: f64,
    written_checks: Vec<String>,
    ledger: Vec<EventLedgerEntry>,
}

/// Entry in event ledger
#[derive(Debug, Serialize, Deserialize)]
pub struct EventLedgerEntry {
    description: String,
    amount: f64,
}
impl EventLedgerEntry {
    fn new(description: &str, amount: f64) -> Self {
        Self {
            description: description.to_string(),
            amount,
        }
    }
}

impl View<Ledger> for LedgerView {
    fn update(&mut self, event: &EventEnvelope<Ledger>) {
        match &event.payload {
            LedgerEvent::CustomerDepositedMoney { amount, balance } => {
                self.ledger.push(EventLedgerEntry::new("deposit", *amount));
                self.balance = *balance;
            }
            LedgerEvent::CustomerWithdrewMoney { amount, balance } => {
                self.ledger
                    .push(EventLedgerEntry::new("withdrawal", *amount));
                self.balance = *balance;
            }
            LedgerEvent::AccountOpened { account_id } => {
                self.account_id = Some(account_id.clone());
            }
        }
    }
}

/// Create CQRS framework for Ledger with Postgres
pub fn cqrs_framework(pool: Pool<Postgres>) -> Arc<PostgresCqrs<Ledger>> {
    // A very simple query that writes each event to stdout.
    let simple_query = SimpleLoggingQuery {};

    // Configure a postgres backed query repository for use in backing a GenericQuery
    // will store serialized views in a Postgres table named identically to the view_name value provided
    // the view_name table should already be defined, see k8s/init.sql
    let account_view_repo = Arc::new(PostgresViewRepository::new("account_query", pool.clone()));

    // A query that stores the current state of an individual account.
    let mut account_query = AccountQuery::new(account_view_repo.clone());

    // Without a query error handler there will be no indication if an
    // error occurs (e.g., database connection failure, missing columns or table).
    // Consider logging an error or panicking in your own application.
    account_query.use_error_handler(Box::new(|e| println!("{}", e)));

    // Create and return an event-sourced `CqrsFramework`.
    let queries: Vec<Box<dyn Query<Ledger>>> =
        vec![Box::new(simple_query), Box::new(account_query)];
    let services = LedgerServices::new(Box::new(HappyPathLedgerServices));

    Arc::new(postgres_es::postgres_cqrs(pool, queries, services))
}

#[cfg(test)]
mod eventstore_tests {
    use super::*;
    use crate::ledger::LedgerCommand;
    use postgres_es::default_postgress_pool;

    const TEST_CONNECTION_STRING: &str = "postgresql://test_user:test_pass@postgres:5432/test";

    #[tokio::test]
    async fn test_db_connect() {
        // The needed database tables are automatically configured with:
        // `cd k8s && kustomize build | podman kube play -`
        // see init file at `k8s/init.sql` for more.
        let pool = default_postgress_pool(TEST_CONNECTION_STRING).await;
        let cqrs = cqrs_framework(pool);
        let command = LedgerCommand::OpenAccount {
            account_id: "TestAccount".to_string(),
        };
        let exe_result = cqrs.execute("test-id", command).await;
        exe_result.expect("Could not create account in DB!");
    }
}
