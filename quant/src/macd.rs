use yahoo_finance_api as yahoo;

/// Get close from quotes
pub fn get_closes(quotes: Vec<yahoo::Quote>) -> Vec<f64> {
    quotes.iter().map(|q| q.close).collect()
}

/// Exponential moving average over N quotes
/// output.len() == quotes.len() - N
pub fn exponential_moving_average<const N: usize>(closes: &Vec<f64>) -> Vec<f64> {
    assert!(!closes.is_empty());
    let alpha: f64 = 2.0 / (N as f64 + 1.0);
    let mut sums: Vec<f64> = vec![0.0; closes.len()];

    sums[0] = closes[0];

    for i in 1..sums.len() {
        let prev = sums[i - 1];
        let close = closes[i];
        sums[i] = alpha * close + (1.0 - alpha) * prev;
    }
    sums
}

/// Get the MACD line, i.e. EMA_12 - EMA_26
pub fn macd_line(closes: &Vec<f64>) -> Vec<f64> {
    let ema_12 = exponential_moving_average::<12>(closes);
    let ema_26 = exponential_moving_average::<26>(closes);
    ema_12
        .iter()
        .zip(ema_26.iter())
        .map(|(ema12, ema26)| ema12 - ema26)
        .collect()
}

/// Get the signal line, i.e. EMA_9 of the MACD line
pub fn macd_signal_line(macd_line: &Vec<f64>) -> Vec<f64> {
    exponential_moving_average::<9>(macd_line)
}

/// Get the MACD histogram, i.e. MACD - Signal
pub fn macd_hist(macd_line: &[f64], macd_signal_line: &[f64]) -> Vec<f64> {
    macd_line
        .iter()
        .zip(macd_signal_line.iter())
        .map(|(macd, signal)| macd - signal)
        .collect()
}

/// MACD
pub fn macd(closes: &Vec<f64>) -> (Vec<f64>, Vec<f64>, Vec<f64>) {
    let macd_line = macd_line(closes);
    let signal_line = macd_signal_line(&macd_line);
    let hist = macd_hist(&macd_line, &signal_line);
    (macd_line, signal_line, hist)
}

#[cfg(test)]
mod tests {
    use super::*;

    use time::macros::datetime;
    use tokio_test::block_on;

    fn assert_almost_eq(a: f64, b: f64, tol: f64) {
        assert!((a - b).abs() < tol, "{} was expected to be {}", a, b);
    }

    async fn get_history_until_jan2020_quotes(
        symbol: &str,
    ) -> Result<Vec<yahoo::Quote>, yahoo::YahooError> {
        let provider = yahoo::YahooConnector::new();
        let start = datetime!(1900-01-01 0:00:00.00 UTC);
        let end = datetime!(2020-01-31 23:59:59.99 UTC);
        // returns historic quotes with daily interval
        let resp = provider.get_quote_history(symbol, start, end).await;
        resp?.quotes()
    }

    #[test]
    fn macd_test() {
        let quotes = block_on(get_history_until_jan2020_quotes("AAPL")).expect("YahooError!");
        let closes = get_closes(quotes);

        let (macd_line, signal_line, hist) = macd(&closes);

        let expected_macd = vec![
            2.18, 2.23, 2.29, 2.28, 2.35, 2.50, 2.61, 2.79, 2.82, 2.78, 2.80, 2.85, 2.82, 2.78,
            2.75, 2.67, 2.40, 2.33, 2.38, 2.39, 2.08,
        ];
        let result_len = expected_macd.len();
        assert!(macd_line.len() >= result_len);
        let actual_start = macd_line.len() - result_len;
        for i in 0..result_len {
            let actual = macd_line[actual_start + i];
            assert_almost_eq(actual, expected_macd[i], 0.01);
        }

        // Numbers from signal line, i.e. MACD EMA_9, so we will also have to verify EMA_12 and EMA_26 of the stock
        let expected_signal = vec![
            1.83, 1.91, 1.99, 2.05, 2.11, 2.19, 2.27, 2.37, 2.46, 2.53, 2.58, 2.64, 2.67, 2.69,
            2.70, 2.70, 2.64, 2.58, 2.54, 2.51, 2.42,
        ];
        let result_len = expected_signal.len();
        assert!(signal_line.len() >= result_len);
        let actual_start = signal_line.len() - result_len;
        for i in 0..result_len {
            let actual = signal_line[actual_start + i];
            assert_almost_eq(actual, expected_signal[i], 0.01);
        }

        let expected_hist = vec![
            0.35, 0.32, 0.3, 0.24, 0.24, 0.32, 0.34, 0.42, 0.36, 0.26, 0.22, 0.21, 0.14, 0.08,
            0.04, -0.02, -0.24, -0.25, -0.15, -0.12, -0.34,
        ];
        let result_len = expected_hist.len();
        assert!(hist.len() >= result_len);
        let actual_start = hist.len() - result_len;
        for i in 0..result_len {
            let actual = hist[actual_start + i];
            assert_almost_eq(actual, expected_hist[i], 0.01);
        }
    }
}
