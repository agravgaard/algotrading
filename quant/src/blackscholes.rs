//! A library for options pricing algorithms

use statrs::distribution::{Continuous, ContinuousCDF, Normal};
use statrs::StatsError;

type InterestRate = f64;
type ExercisePrice = f64;
type Volatility = f64;
type CallValue = f64;

/// Type alias for time, unit is years
pub type Time = f64;
/// Type alias for price of the undelying, unit shouldn't matter, just be consistent
pub type StockPrice = f64;

/// # Black-Scholes equation
/// ```text
///       dC                          d^2C    dC
///   rS ---- + 0.5 * sigma^2 * S^2 * ---- + ---- = rC
///       dS                          dS^2    dt
/// <=>
///   rS * Delta + 0.5 * sigma^2 * S^2 * Gamma + Theta = rC
/// ```
///
/// S: stock price
/// t: time to expiration (in years)
/// C: theoretical value of a European call
/// P: theoretical value of a European put
/// sigma: volatility (annual SD)
/// r: annual interest rate
///
/// X: exercise price
/// N(): normal cdf
///
/// ```text
/// C =   S * N( d1) - X * e^(-rt) * N( d2)
/// P = - S * N(-d1) + X * e^(-rt) * N(-d2)
/// ```
///
/// ```text
/// d1 = { ln(S/X) + t * [r + (sigma^2 / 2)] } / { sigma * sqrt(t) }
/// d2 = { ln(S/X) + t * [r - (sigma^2 / 2)] } / { sigma * sqrt(t) } = d1 - sigma * sqrt(t)
/// ```
///
/// Put-Call parity:
/// ```text
/// C - P = ( F - X ) / ( 1 + r * t )
/// ```
///
/// Forward price:
/// ```text
/// F = S * (1 + r * t)
/// ```
/// With continuous interest:
/// ```text
///   = S * e^(rt)
/// ```
///
/// ```text
/// => C - P = S - X / (1 + r * t)
/// ```
/// With continuous interest:
/// ```text
///          = S - X * e^(-rt)
/// ```
/// which is the arbitrage boundary.
///
/// To account for dividends replace `S` with `S - sum[D*e^(r*td)]`
/// where td is the time to expiration for each of the dividend payments
///
#[derive(Debug, Copy, Clone)]
pub struct Option {
    /// Exercise / Strike price of the option
    pub x: ExercisePrice,
    /// Risk-free interest rate
    pub r: InterestRate,
    /// The volatility (SD) of the undelying [0, 1]
    pub sigma: Volatility,
    /// b=r for stocks,
    /// b=r=0 for futures with futures-type settlement,
    /// b=0 for futures with stock-type settlement,
    /// b=r-rf on foreign currencies
    pub b: InterestRate,
}

impl Option {
    /// The value of a European call option
    pub fn black_scholes_call(&self) -> Result<impl Fn(StockPrice, Time) -> CallValue, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        Ok(move |s: StockPrice, t: Time| -> CallValue {
            let d1 = d1(x, sigma, b, t, s);
            let d2 = d2(d1, sigma, t);
            s * ((b - r) * t).exp() * n.cdf(d1) - x * (-r * t).exp() * n.cdf(d2)
        })
    }
    /// The value of a European put option
    pub fn black_scholes_put(&self) -> Result<impl Fn(StockPrice, Time) -> CallValue, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        Ok(move |s: StockPrice, t: Time| -> CallValue {
            let d1 = d1(x, sigma, b, t, s);
            let d2 = d2(d1, sigma, t);
            x * (-r * t).exp() * n.cdf(-d2) - s * ((b - r) * t).exp() * n.cdf(-d1)
        })
    }

    /// Delta for a call option
    pub fn delta_call(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        Ok(((b - r) * t).exp() * n.cdf(d1(x, sigma, b, t, s)))
    }
    /// Delta for a put option
    pub fn delta_put(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        Ok(((b - r) * t).exp() * (n.cdf(d1(x, sigma, b, t, s)) - 1.0))
    }

    /// Gamma for any option
    pub fn gamma(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        Ok(((b - r) * t).exp() * n.pdf(d1(x, sigma, b, t, s)) / (s * sigma * t.sqrt()))
    }

    /// Theta for a call option
    /// sensitivity over one year. Divide by 365 to get daily decay.
    pub fn theta_call(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;
        let n = Normal::new(0.0, 1.0)?;
        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        let decay_in_option_volatility_value =
            (-s * ((b - r) * t).exp() * n.pdf(d1) * sigma) / (2.0 * t.sqrt());
        // for an underlying contract such as stock, the spot price is assumed to move toward the forward price as time passes.
        let move_to_forward = (b - r) * s * ((b - r) * t).exp() * n.cdf(d1);
        // the present value of the option’s expected value at expiration is changing as time passes.
        let present_value = r * x * (-r * t).exp() * n.cdf(d2);
        Ok(decay_in_option_volatility_value - move_to_forward - present_value)
    }
    /// Theta for a put option
    /// sensitivity over one year. Divide by 365 to get daily decay.
    pub fn theta_put(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        let decay_in_option_volatility_value =
            (-s * ((b - r) * t).exp() * n.pdf(d1) * sigma) / (2.0 * t.sqrt());
        // for an underlying contract such as stock, the spot price is assumed to move toward the forward price as time passes.
        let move_to_forward = (b - r) * s * ((b - r) * t).exp() * n.cdf(d1);
        // the present value of the option’s expected value at expiration is changing as time passes.
        let present_value = r * x * (-r * t).exp() * n.cdf(-d2);
        Ok(decay_in_option_volatility_value + move_to_forward + present_value)
    }

    /// Vega for any option
    /// i.e. the sensitivity of the option to one full point (100%) change in volatility.
    /// It's commonly divided by 100 for a 1% point change instead
    pub fn vega(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        let d1 = d1(x, sigma, b, t, s);
        Ok(s * ((b - r) * t).exp() * n.pdf(d1) * t.sqrt())
    }

    /// Rho for a call option
    /// i.e. the sensitivity of the option to one full point (100%) change in interest rate.
    /// It's commonly divided by 100 for a 1% point change instead
    pub fn rho_call(&self, t: Time, s: StockPrice, future: bool) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        if future {
            // b == 0
            let c = self.black_scholes_call()?;
            return Ok(-t * c(s, t));
        }
        let n = Normal::new(0.0, 1.0)?;
        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        Ok(t * x * (-r * t).exp() * n.cdf(d2))
    }

    /// Rho for a put option
    /// i.e. the sensitivity of the option to one full point (100%) change in interest rate.
    /// It's commonly divided by 100 for a 1% point change instead
    pub fn rho_put(&self, t: Time, s: StockPrice, future: bool) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        if future {
            // b == 0
            let p = self.black_scholes_put()?;
            return Ok(-t * p(s, t));
        }
        let n = Normal::new(0.0, 1.0)?;
        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        Ok(-t * x * (-r * t).exp() * n.cdf(-d2))
    }

    /// Elasticity for a call option
    pub fn elasticity_call(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let c = self.black_scholes_call()?;
        let delta = self.delta_call(t, s)?;
        Ok(delta * (s / c(s, t)))
    }
    /// Elasticity for a put option
    pub fn elasticity_put(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let p = self.black_scholes_put()?;
        let delta = self.delta_put(t, s)?;
        Ok(delta * (s / p(s, t)))
    }

    /// Vanna for any option
    pub fn vanna(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        Ok(-((b - r) * t).exp() * n.pdf(d1) * (d2 / sigma))
    }

    /// Charm for a call option
    /// Also known as DdeltaDtime
    pub fn charm_call(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        Ok(-((b - r) * t).exp()
            * (n.pdf(d1) * (b / (sigma * t.sqrt()) - d2 / (2.0 * t)) + (b - r) * n.cdf(d1)))
    }

    /// Charm for a put option
    /// Also known as DdeltaDtime
    pub fn charm_put(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let n = Normal::new(0.0, 1.0)?;
        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        Ok(-((b - r) * t).exp()
            * (n.pdf(d1) * (b / (sigma * t.sqrt()) - d2 / (2.0 * t)) - (b - r) * n.cdf(-d1)))
        // There is a sign error in the book, "OPTION VOLATILITY AND PRICING", N(d1) should be
        // N(-d1) for a put option. The book, "The Complete Guide to Option Pricing Formula"(2007),
        // has the correct formula.
    }

    /// Speed for any option
    pub fn speed(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let sigma = self.sigma;
        let b = self.b;

        let d1 = d1(x, sigma, b, t, s);
        let gamma = self.gamma(t, s)?;
        Ok(-(gamma / s) * (1.0 + (d1 / (sigma * t.sqrt()))))
    }

    /// Color for any option
    pub fn color(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        let gamma = self.gamma(t, s)?;
        Ok(gamma * (r - b + (b * d1) / (sigma * t.sqrt()) + (1.0 - d1 * d2) / (2.0 * t)))
    }

    /// Volga (Vomma) for any option
    pub fn volga(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let sigma = self.sigma;
        let b = self.b;

        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        let vega = self.vega(t, s)?;
        Ok(vega * (d1 * d2) / sigma)
    }

    /// Vega Decay for any option
    pub fn vega_decay(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let r = self.r;
        let sigma = self.sigma;
        let b = self.b;

        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        let vega = self.vega(t, s)?;
        Ok(vega * (r - b + (b * d1) / (sigma * t.sqrt()) + (1.0 - d1 * d2) / (2.0 * t)))
    }

    /// Zomma for any option
    pub fn zomma(&self, t: Time, s: StockPrice) -> Result<f64, StatsError> {
        let x = self.x;
        let sigma = self.sigma;
        let b = self.b;

        let d1 = d1(x, sigma, b, t, s);
        let d2 = d2(d1, sigma, t);
        let gamma = self.gamma(t, s)?;
        Ok(gamma * (d1 * d2 - 1.0) / sigma)
    }
}

fn d1(x: ExercisePrice, sigma: Volatility, b: InterestRate, t: Time, s: StockPrice) -> f64 {
    ((s / x).ln() + t * sigma.powi(2).mul_add(0.5, b)) / (sigma * t.sqrt())
}

fn d2(d1: f64, sigma: Volatility, t: Time) -> f64 {
    d1 - sigma * t.sqrt()
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::{Option, StockPrice, Time};

    fn assert_almost_eq(a: f64, b: f64) {
        assert!((a - b).abs() < 0.0001, "{} was expected to be {}", a, b);
    }

    #[test]
    fn d1_test() {
        let sigma: Volatility = 0.01;
        let t: Time = 1.0; // one year
        let x: ExercisePrice = 65.0;
        let s: StockPrice = 65.0;
        let d1 = d1(x, sigma, 0.0, t, s);
        assert_almost_eq(d1, 0.005);
        let n = Normal::new(0.0, 1.0).unwrap();
        assert_almost_eq(n.cdf(d1), 0.501995);
    }

    #[test]
    fn d2_test() {
        let sigma: Volatility = 0.01;
        let t: Time = 1.0; // one year
        let x: ExercisePrice = 65.0;
        let s: StockPrice = 65.0;
        let d1 = d1(x, sigma, 0.0, t, s);
        let d2 = d2(d1, sigma, t);
        assert_almost_eq(d2, -0.005);
        let n = Normal::new(0.0, 1.0).unwrap();
        assert_almost_eq(n.cdf(d2), 0.498005);
    }

    #[test]
    fn black_scholes_test() {
        let option = Option {
            x: 100.0,
            r: 0.0,
            sigma: 0.01,
            b: 0.0,
        };
        let call = option.black_scholes_call().unwrap();
        let t: Time = 1.0; // one year
        let s: StockPrice = 100.0; // "at-the-forward"
        let call_result = call(s, t);
        let call_expect = option.x * 0.003990;
        assert_almost_eq(call_result, call_expect);
    }

    #[test]
    fn delta_call_test() {
        let mut option = Option {
            x: 90.0,
            sigma: 27.0 / 100.0,
            r: 0.0,
            b: 0.0,
        };
        let s: StockPrice = 101.25;
        let t: Time = 6.0 / 52.0;
        let mut delta = option.delta_call(t, s).unwrap();
        assert_almost_eq(delta, 0.908256);
        option.x = 95.0;
        delta = option.delta_call(t, s).unwrap();
        assert_almost_eq(delta, 0.770525);
        option.r = 0.01;
        option.b = option.r; // option on stock, b = r
        delta = option.delta_call(t, s).unwrap();
        assert_almost_eq(delta, 0.774322);
    }

    #[test]
    fn delta_put_test() {
        let mut option = Option {
            x: 90.0,
            sigma: 27.0 / 100.0,
            r: 0.0,
            b: 0.0,
        };
        let s: StockPrice = 101.25;
        let t: Time = 6.0 / 52.0;
        let mut delta = option.delta_put(t, s).unwrap();
        assert_almost_eq(delta, -0.091744);
        option.x = 95.0;
        delta = option.delta_put(t, s).unwrap();
        assert_almost_eq(delta, -0.229475);
        option.r = 0.01;
        option.b = option.r; // option on stock, b = r
        delta = option.delta_put(t, s).unwrap();
        assert_almost_eq(delta, -0.225678);
    }

    #[test]
    fn gamma_test() {
        let mut option = Option {
            x: 90.0,
            sigma: 27.0 / 100.0,
            r: 0.0,
            b: 0.0,
        };
        let s: StockPrice = 101.25;
        let t: Time = 6.0 / 52.0;
        let mut gamma = option.gamma(t, s).unwrap();
        assert_almost_eq(gamma, 0.01773828);
        option.x = 95.0;
        gamma = option.gamma(t, s).unwrap();
        assert_almost_eq(gamma, 0.03265743);
        option.r = 0.01;
        option.b = option.r; // option on stock, b = r
        gamma = option.gamma(t, s).unwrap();
        assert_almost_eq(gamma, 0.03235201);
    }

    #[test]
    fn theta_call_test() {
        let mut option = Option {
            x: 90.0,
            sigma: 27.0 / 100.0,
            r: 0.0,
            b: 0.0,
        };
        let s: StockPrice = 101.25;
        let t: Time = 6.0 / 52.0;
        let mut theta_call = option.theta_call(t, s).unwrap();
        assert_almost_eq(theta_call, -6.62825173);
        option.x = 95.0;
        theta_call = option.theta_call(t, s).unwrap();
        assert_almost_eq(theta_call, -12.20308539);
        option.r = 0.01;
        option.b = option.r; // option on stock, b = r
        theta_call = option.theta_call(t, s).unwrap();
        assert_almost_eq(theta_call, -12.79668654);
    }

    #[test]
    fn theta_put_test() {
        let mut option = Option {
            x: 90.0,
            sigma: 27.0 / 100.0,
            r: 0.0,
            b: 0.0,
        };
        let s: StockPrice = 101.25;
        let t: Time = 6.0 / 52.0;
        let mut theta_put = option.theta_put(t, s).unwrap();
        assert_almost_eq(theta_put, -6.62825173);
        option.x = 95.0;
        theta_put = option.theta_put(t, s).unwrap();
        assert_almost_eq(theta_put, -12.20308539);
        option.r = 0.01;
        option.b = option.r; // option on stock, b = r
        theta_put = option.theta_put(t, s).unwrap();
        assert_almost_eq(theta_put, -11.84778206);
    }

    #[test]
    fn vega_test() {
        let mut option = Option {
            x: 90.0,
            sigma: 27.0 / 100.0,
            r: 0.0,
            b: 0.0,
        };
        let s: StockPrice = 101.25;
        let t: Time = 6.0 / 52.0;
        let mut vega = option.vega(t, s).unwrap();
        assert_almost_eq(vega, 5.66517242);
        option.x = 95.0;
        vega = option.vega(t, s).unwrap();
        assert_almost_eq(vega, 10.42998751);
        option.r = 0.01;
        option.b = option.r; // option on stock, b = r
        vega = option.vega(t, s).unwrap();
        assert_almost_eq(vega, 10.33244395);
    }

    #[test]
    fn rho_call_test() {
        let mut option = Option {
            x: 90.0,
            sigma: 27.0 / 100.0,
            r: 0.0,
            b: 0.0,
        };
        let future = false; // this example is for a stock option
        let s: StockPrice = 101.25;
        let t: Time = 6.0 / 52.0;
        let mut rho_call = option.rho_call(t, s, future).unwrap();
        assert_almost_eq(rho_call, 9.26528065);
        option.x = 95.0;
        rho_call = option.rho_call(t, s, future).unwrap();
        assert_almost_eq(rho_call, 8.13111993);
        option.r = 0.01;
        option.b = option.r; // option on stock, b = r
        rho_call = option.rho_call(t, s, future).unwrap();
        assert_almost_eq(rho_call, 8.16608217);
    }

    #[test]
    fn rho_put_test() {
        let mut option = Option {
            x: 90.0,
            sigma: 27.0 / 100.0,
            r: 0.0,
            b: 0.0,
        };
        let future = false; // this example is for a stock option
        let s: StockPrice = 101.25;
        let t: Time = 6.0 / 52.0;
        let mut rho_put = option.rho_put(t, s, future).unwrap();
        assert_almost_eq(rho_put, -1.11933473);
        option.x = 95.0;
        rho_put = option.rho_put(t, s, future).unwrap();
        assert_almost_eq(rho_put, -2.83041853);
        option.r = 0.01;
        option.b = option.r; // option on stock, b = r
        rho_put = option.rho_put(t, s, future).unwrap();
        assert_almost_eq(rho_put, -2.78281566);
    }

    #[test]
    fn elasticity_call_test() {
        let option = Option {
            x: 100.0,
            sigma: 0.36,
            r: 0.1,
            b: 0.0,
        };
        let s: StockPrice = 105.0;
        let t: Time = 0.5;
        let elasticity_call = option.elasticity_call(t, s).unwrap();
        // I don't have a third party example...
        assert_almost_eq(elasticity_call, 5.021860382027567);
    }

    #[test]
    fn elasticity_put_test() {
        let option = Option {
            x: 100.0,
            sigma: 0.36,
            r: 0.1,
            b: 0.0,
        };
        let s: StockPrice = 105.0;
        let t: Time = 0.5;
        let elasticity_put = option.elasticity_put(t, s).unwrap();
        assert_almost_eq(elasticity_put, -4.8775);
    }

    #[test]
    fn charm_call_test() {
        let option = Option {
            x: 90.0,
            sigma: 0.24,
            r: 0.14,
            b: 0.0,
        };
        let s: StockPrice = 105.0;
        let t: Time = 3.0 / 12.0;
        let charm_call = option.charm_call(t, s).unwrap();
        // I don't have a third party example...
        assert_almost_eq(charm_call, 0.505174210799453);
    }

    #[test]
    fn charm_put_test() {
        let option = Option {
            x: 90.0,
            sigma: 0.24,
            r: 0.14,
            b: 0.0,
        };
        let s: StockPrice = 105.0;
        let t: Time = 3.0 / 12.0;
        let charm_put = option.charm_put(t, s).unwrap();
        assert_almost_eq(charm_put, 0.3700);
    }

    #[test]
    fn vanna_test() {
        let option = Option {
            x: 80.0,
            sigma: 0.20,
            r: 0.05,
            b: 0.05,
        };
        let s: StockPrice = 90.0;
        let t: Time = 3.0 / 12.0;
        let vanna = option.vanna(t, s).unwrap();
        assert_almost_eq(vanna, -1.0008);
    }

    #[test]
    fn speed_test() {
        let option = Option {
            x: 48.0,
            sigma: 0.20,
            r: 0.06,
            b: 0.01,
        };
        let s: StockPrice = 50.0;
        let t: Time = 1.0 / 12.0;
        let speed = option.speed(t, s).unwrap();
        assert_almost_eq(speed, -0.0291);
    }

    #[test]
    fn color_test() {
        let option = Option {
            x: 80.0,
            sigma: 0.20,
            r: 0.05,
            b: 0.05,
        };
        let s: StockPrice = 90.0;
        let t: Time = 3.0 / 12.0;
        let color = option.color(t, s).unwrap();
        // I don't have a third party example...
        assert_almost_eq(color, -0.012663074066683832);
    }

    #[test]
    fn volga_test() {
        let option = Option {
            x: 130.0,
            sigma: 0.28,
            r: 0.05,
            b: 0.0,
        };
        let s: StockPrice = 90.0;
        let t: Time = 9.0 / 12.0;
        let volga = option.volga(t, s).unwrap();
        assert_almost_eq(volga, 92.3444);
    }

    #[test]
    fn vega_decay_test() {
        let option = Option {
            x: 80.0,
            sigma: 0.20,
            r: 0.05,
            b: 0.05,
        };
        let s: StockPrice = 90.0;
        let t: Time = 3.0 / 12.0;
        let vega_decay = option.vega_decay(t, s).unwrap();
        // I don't have a third party example...
        assert_almost_eq(vega_decay, -5.128544997006951);
    }

    #[test]
    fn zomma_test() {
        let option = Option {
            x: 80.0,
            sigma: 0.26,
            r: 0.05,
            b: 0.00,
        };
        let s: StockPrice = 100.0;
        let t: Time = 3.0 / 12.0;
        let zomma = option.zomma(t, s).unwrap();
        assert_almost_eq(zomma, 0.0463);
    }
}
