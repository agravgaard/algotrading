//! A library for options pricing algorithms

#![doc(html_root_url = "https://gitlab.com/agravgaard/algotrading")]
#![forbid(unsafe_code)]
#![deny(
    absolute_paths_not_starting_with_crate,
    elided_lifetimes_in_paths,
    explicit_outlives_requirements,
    keyword_idents,
    macro_use_extern_crate,
    meta_variable_misuse,
    missing_abi,
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    non_ascii_idents,
    noop_method_call,
    pointer_structural_match,
    single_use_lifetimes,
    trivial_casts,
    trivial_numeric_casts,
    unreachable_pub,
    unsafe_op_in_unsafe_fn,
    unstable_features,
    unused_crate_dependencies,
    unused_extern_crates,
    unused_import_braces,
    unused_lifetimes,
    unused_qualifications,
    unused_results,
    variant_size_differences
)]
#![warn(
    clippy::panic,
    future_incompatible,
    nonstandard_style,
    rust_2018_idioms
)]

/// Historic volatility and other basic analysis methods
pub mod basic_analysis;

/// Black-Scholes greeks and premium models
pub mod blackscholes;

/// Moving Average Convergence/Divergence
pub mod macd;

/// Calculate the premium from the Delta
pub fn delta_premium(delta: f64, underlying_diff: f64) -> f64 {
    underlying_diff * delta
}

/// Calculate new Delta from Gamma and difference in the underlying
pub fn get_new_delta(gamma: f64, underlying_diff: f64) -> f64 {
    gamma * underlying_diff
}

/// Calculate the premium from the Vega and difference in volatility
pub fn vega_premium(volatility_diff: f64, vega: f64) -> f64 {
    (volatility_diff * 100.0) * vega
}

#[cfg(test)]
mod tests {
    use super::*;

    fn assert_almost_eq(a: f64, b: f64) {
        assert!((a - b).abs() < 0.0001, "{} was expected to be {}", a, b);
    }

    #[test]
    fn delta_premium_test() {
        let premium = 1.50;
        // Calls always have positive delta between 0 and 1, puts negative -1 to 0
        let delta = 0.40;
        let underlying_diff = 102.0 - 100.0;

        let new_premium = premium + delta_premium(delta, underlying_diff);
        assert_almost_eq(new_premium, 2.3);
    }
    #[test]
    fn vega_premium_test() {
        let new_vega_premium = vega_premium(0.015, 0.12) + 7.5;
        assert_almost_eq(new_vega_premium, 7.68);
    }
    #[test]
    fn e2e_premium_test() {
        let underlying_future_price = 980.0;
        let call_premium = 12.0;
        let volatility_today = 0.15;
        let delta_today = 0.40;
        let theta_today = -0.20;
        let vega_today = 0.10;
        let gamma_today = 0.5;
        let time_diff = 14.0; // days
        let underlying_future_price_later = 1000.0;
        let volatility_later = 0.14;

        let underlying_future_diff = underlying_future_price_later - underlying_future_price;
        let delta_diff = get_new_delta(gamma_today, underlying_future_diff);
        assert_almost_eq(delta_diff, 10.0);

        let avg_delta = delta_today + (delta_diff / 100.0) / 2.0;
        assert_almost_eq(avg_delta, 0.45);

        let new_delta_premium = delta_premium(avg_delta, underlying_future_diff);
        assert_almost_eq(new_delta_premium, 9.0);

        let new_theta_premium = theta_today * time_diff;
        assert_almost_eq(new_theta_premium, -2.8);

        let new_vega_premium = vega_premium(volatility_later - volatility_today, vega_today);
        assert_almost_eq(new_vega_premium, -0.1);

        let new_call_premium =
            call_premium + new_delta_premium + new_theta_premium + new_vega_premium;
        assert_almost_eq(new_call_premium, 18.1);
    }
}
