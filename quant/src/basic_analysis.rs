use yahoo_finance_api as yahoo;

/// Volume adjusted price average based on the adjusted close
pub fn adjusted_average(quotes: Vec<yahoo::Quote>) -> f64 {
    let (close_vol_sum, vol_sum) = quotes.iter().fold(
        (0.0, 0),
        |lhs: (f64, u64), rhs: &yahoo::Quote| -> (f64, u64) {
            (
                lhs.0 + (rhs.adjclose * rhs.volume as f64),
                lhs.1 + rhs.volume,
            )
        },
    );
    close_vol_sum / vol_sum as f64
}

/// Calculates the residuals, i.e. ln(C_i/C_{i-1}), based on close
pub fn log_returns(quotes: Vec<yahoo::Quote>) -> Vec<f64> {
    quotes
        .windows(2)
        .map(|q| -> f64 { (q[1].close / q[0].close).ln() })
        .collect()
}

/// Volatility based on close
pub fn historic_volatility(quotes: Vec<yahoo::Quote>) -> f64 {
    let n_obs: f64 = 252.0; // Annualize from daily
    let log_returns: Vec<f64> = log_returns(quotes);
    let sum_returns: f64 = log_returns.iter().fold(0.0, |lhs, rhs| lhs + rhs);
    let mean = sum_returns / (log_returns.len() as f64);

    let sum_of_squared_deviation: f64 = log_returns
        .iter()
        .fold(0.0, |lhs, rhs| -> f64 { lhs + (rhs - mean).powi(2) });
    let variance = sum_of_squared_deviation / (log_returns.len() as f64 - 1.0);
    (variance * n_obs).sqrt()
}

/// Rogers-Satchell Volatility based on open, close, high and low
pub fn rogers_satchell_volatility(quotes: &Vec<yahoo::Quote>) -> f64 {
    let freq: f64 = 252.0; // Frequency of returns in a year, i.e. daily
    let n_obs: f64 = quotes.len() as f64;

    let sum_of_logs: f64 = quotes.iter().fold(0.0, |sum, q| -> f64 {
        sum + (q.high / q.close).ln() * (q.high / q.open).ln()
            + (q.low / q.close).ln() * (q.low / q.open).ln()
    });

    let factor = (freq / n_obs).sqrt();
    factor * sum_of_logs.sqrt()
}

fn variance_overnight(quotes: &Vec<yahoo::Quote>) -> f64 {
    let n_obs: f64 = quotes.len() as f64;
    let sum_of_log_o_c: f64 = quotes
        .windows(2)
        .fold(0.0, |sum, q| -> f64 { sum + (q[1].open / q[0].close).ln() });
    let mean_log_o_c: f64 = sum_of_log_o_c / (n_obs - 1.0);
    let sum_of_sqdev_log_o_c: f64 = quotes.windows(2).fold(0.0, |sum, q| -> f64 {
        sum + ((q[1].open / q[0].close).ln() - mean_log_o_c).powi(2)
    });
    sum_of_sqdev_log_o_c / (n_obs - 1.0)
}

fn variance_open_to_close(quotes: &Vec<yahoo::Quote>) -> f64 {
    let n_obs: f64 = quotes.len() as f64;
    let sum_of_log_c_o: f64 = quotes
        .iter()
        .fold(0.0, |sum, q| -> f64 { sum + (q.close / q.open).ln() });
    let mean_log_c_o: f64 = sum_of_log_c_o / (n_obs - 1.0);
    let sum_of_sqdev_log_c_o: f64 = quotes.iter().fold(0.0, |sum, q| -> f64 {
        sum + ((q.close / q.open).ln() - mean_log_c_o).powi(2)
    });
    sum_of_sqdev_log_c_o / (n_obs - 1.0)
}

/// Yang-Zhang Volatility based on open, close, high and low
pub fn yang_zhang_volatility(quotes: &Vec<yahoo::Quote>) -> f64 {
    let freq: f64 = 252.0; // Frequency of returns in a year, i.e. daily
    let n_obs: f64 = quotes.len() as f64;
    let k: f64 = 0.34 / (1.34 + (n_obs + 1.0) / (n_obs - 1.0));

    let s_rs = rogers_satchell_volatility(quotes);
    let sq_s_overnight: f64 = freq * variance_overnight(quotes);
    let sq_s_open_to_close: f64 = freq * variance_open_to_close(quotes);

    (sq_s_overnight + k * sq_s_open_to_close + (1.0 - k) * s_rs.powi(2)).sqrt()
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::time::{Duration, UNIX_EPOCH};
    use time::{macros::datetime, OffsetDateTime};
    use tokio_test::block_on;

    fn assert_almost_eq(a: f64, b: f64, tol: f64) {
        assert!((a - b).abs() < tol, "{} was expected to be {}", a, b);
    }

    async fn get_jan2020_quotes(symbol: &str) -> Result<Vec<yahoo::Quote>, yahoo::YahooError> {
        let provider = yahoo::YahooConnector::new();
        let start = datetime!(2020-01-01 0:00:00.00 UTC);
        let end = datetime!(2020-01-31 23:59:59.99 UTC);
        // returns historic quotes with daily interval
        let resp = provider.get_quote_history(symbol, start, end).await;
        resp?.quotes()
    }

    async fn get_feb2016_quotes(symbol: &str) -> Result<Vec<yahoo::Quote>, yahoo::YahooError> {
        // It has 20 bank-days
        let provider = yahoo::YahooConnector::new();
        let start = datetime!(2016-02-01 0:00:00.00 UTC);
        let end = datetime!(2016-02-29 23:59:59.99 UTC);
        // returns historic quotes with daily interval
        let resp = provider.get_quote_history(symbol, start, end).await;
        resp?.quotes()
    }

    #[test]
    fn adjusted_average_test() {
        let quotes = block_on(get_jan2020_quotes("AAPL")).expect("YahooError!");
        let avg = adjusted_average(quotes);
        println!("The 2020/01/* avg price of Apple was {}", avg);
        assert_almost_eq(avg, 76.12, 0.5);
    }
    #[test]
    fn historic_volatility_test() {
        let quotes = block_on(get_feb2016_quotes("AAPL")).expect("YahooError!");
        assert!(
            quotes.len() == 20,
            "There is 20 working days in Feb 2016, quotes must reflect that"
        );
        let sigma = historic_volatility(quotes);
        println!("The volatility of Apple for Feb 2016 was {}", sigma);
        // 23.37 was calculated with TradingView (modifying to 252 days per year).
        // Their data might be slightly different, so we accept an error up to 0.01
        assert_almost_eq(sigma, 0.2337, 0.01);
    }

    #[test]
    fn yang_zhang_volatility_test() {
        let quotes = block_on(get_feb2016_quotes("AAPL")).expect("YahooError!");
        assert!(
            quotes.len() == 20,
            "There is 20 working days in Feb 2016, quotes must reflect that"
        );
        let sigma = yang_zhang_volatility(&quotes);
        println!(
            "The Yang-Zhang volatility of Apple for Feb 2016 was {}",
            sigma
        );
        assert_almost_eq(sigma, 0.2, 0.1);
    }

    #[test]
    fn latest_quote_test() {
        let provider = yahoo::YahooConnector::new();
        // get the latest quotes in 1 minute intervals
        let response = block_on(provider.get_latest_quotes("AAPL", "1d")).unwrap();
        // extract just the latest valid quote summery
        // including timestamp,open,close,high,low,volume
        let quote = response.last_quote().unwrap();
        let time = OffsetDateTime::from(UNIX_EPOCH + Duration::from_secs(quote.timestamp));
        println!("At {} quote price of Apple was {}", time, quote.close);
    }

    // Search for a ticker given a search string (e.g. company name):
    #[test]
    fn search_ticker_test() {
        let provider = yahoo::YahooConnector::new();
        let resp = block_on(provider.search_ticker("Apple")).unwrap();

        println!("All tickers found while searching for 'Apple':");
        for item in resp.quotes {
            println!("{}", item.symbol)
        }
    }
}
